﻿using DocumentFormat.OpenXml.Bibliography;
using DocumentFormat.OpenXml.Spreadsheet;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoExportExcel
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Instancia del contexto de la base de datos
            using (Modelo.UsuariosEntities db = new Modelo.UsuariosEntities())
            {
                // Consulta para listar los objetos
                dataGridView1.DataSource = (
                    from d in db.Usuarios
                    select new Modelo.ViewModelo.UsuarioViewModelo
                    {
                        Id = d.IdCliente,
                        Nombre = d.Nombre,
                        Apellido = d.Apellido,
                        Documento = d.Documento,
                        Correo = d.Correo,
                        Direccion = d.Direccion
                    }).ToList();
            }
        }

        // Boton para exportar archivo excel
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                // Cuadro de dialogo para seleccionar la ubicacion y nombre del archivo
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Archivos de Excel (*.xlsx)|*.xlsx"; // Seleccionar archivos solo con la extension .xlsx
                saveFileDialog1.Title = "Guardar archivo de Excel";

                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    // Se crea una instancia de la clase ExcelPackage que nos proporciona EPPlus para trabajar archivos excel
                    using (var package = new ExcelPackage())
                    {
                        // Hoja de trabajo excel
                        ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Usuarios");

                        // Encabezados de columna
                        for (int i = 0; i < dataGridView1.Columns.Count; i++)
                        {
                            // Primer valor representa numero de fila, segundo valor representa la columna
                            worksheet.Cells[1, i + 1].Value = dataGridView1.Columns[i].HeaderText;

                            // Formato en negrita al encabezado de la hoja
                            worksheet.Cells[1, i + 1].Style.Font.Bold = true;
                        }

                        // Datos de la grilla
                        for (int i = 0; i < dataGridView1.Rows.Count; i++)
                        {
                            for (int j = 0; j < dataGridView1.Columns.Count; j++)
                            {
                                // Comenzamos en la fila 2 de la hoja
                                worksheet.Cells[i + 2, j + 1].Value = dataGridView1.Rows[i].Cells[j].Value;
                            }
                        }

                        // Guardando excel en la ubicacion especificada
                        FileInfo fileInfo = new FileInfo(saveFileDialog1.FileName);
                        package.SaveAs(fileInfo);
                    }

                    MessageBox.Show("Los datos de los usuarios se han exportado exitosamente en un archivo Excel.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error al exportar el archivo excel: " + ex.Message);
            }
        }
    }
}
